# This is a sample Python script.

# Press Mayús+F10 to execute it or replace it with your code.
# Press Double Shift to search everywhere for classes, files, tool windows, actions, and settings.
import csv
import json
import statistics




def print_hi():

    registros = []
    str_registo = "{"
    registros_item = []
    registro_regiones_tem = [[],[],[],[],[],[]]
    registro_regiones_pre = [[], [], [], [], [],[]]
    registro_regiones_pres_avg = {"1": 0, "2": 0, "3": 0, "4": 0, "5": 0}
    registro_regiones_tem_avg= {"1": 0, "2": 0, "3": 0, "4": 0, "5": 0}
    #se abre el archivo csv
    with open('data.csv','r') as csvfile:
        lector = csv.reader(csvfile)

        cabecera = next(lector)
        for file in lector:
            registros.append(file)
    #armado del json
        for regiones,value in enumerate(registros):
            registro_regiones_tem[int(value[1])].append(int(value[2]))
            registro_regiones_pre[int(value[1])].append(int(value[3]))
           # escritura de registros
        for i in range(1,6):
            registro_regiones_tem_avg[f"{i}"] = round(float(statistics.mean(registro_regiones_tem[i])),1)
            registro_regiones_pres_avg[f"{i}"] = round(float(statistics.mean(registro_regiones_pre[i])),1)
    for i,value in enumerate(range(1,6)):
       registros_item.append(float(registro_regiones_tem_avg[f"{i+1}"]))
       registros_item.append(float(registro_regiones_pres_avg[f"{i+1}"]))
       str_registo += f"\"{str(i+1)}\":{registros_item},"
       registros_item = []
    str_registo = str_registo[:-1]
    str_registo +="}"

    str_registo_final = str_registo.replace(" ","")
    datos = json.loads(str_registo_final)
    datos = json.dumps(datos, indent=4, sort_keys=True)
    #fin del armado del json
    #creacion del nuevo archivo csv
    with open('data_nuevo.csv', 'w') as csvfile2:
        escritor = csv.writer(csvfile2)
        cabecera.append("above_avg_temp")
        cabecera.append("above_avg_pres")
        escritor.writerow(cabecera)
        # escritura de registros
        for index, value in enumerate(registros):
            if int(value[2]) > int(registro_regiones_tem_avg[f"{value[1]}"]):
                registros[index].append("SI")
            else:
                registros[index].append("NO")
            if int(value[3]) > int(registro_regiones_pres_avg[f"{value[1]}"]):
                registros[index].append("SI")
            elif int(value[3]) == int(registro_regiones_pres_avg[f"{value[1]}"]) and value[1] == '2'  :
                registros[index].append("IGUAL")
            else:
                registros[index].append("NO")
        escritor.writerows(registros)




    #print(datos)
    #print(type(datos))




# Press the green button in the gutter to run the script.
if __name__ == '__main__':
    print_hi()

# See PyCharm help at https://www.jetbrains.com/help/pycharm/
